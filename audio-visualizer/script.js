
const audioPlayer = document.querySelector('audio');

audioPlayer.addEventListener('play', () => {

  const contexteAudio = new AudioContext();
  const src = contexteAudio.createMediaElementSource(audioPlayer);
  const analyseur = contexteAudio.createAnalyser();

  const canvas = document.getElementById('canvas');
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  const ctx = canvas.getContext('2d');

  src.connect(analyseur);
  analyseur.connect(contexteAudio.destination);

  analyseur.fftSize = 1024; // fft veut dire Transformation de Fourier Rapide -> ça permet d'obtenir une fréquence à partir d'un son
  const frequencesAudio = analyseur.frequencyBinCount;
  //console.log(frequencesAudio);
  const tableauFrequences = new Uint8Array(frequencesAudio);

  const WIDTH = canvas.width;
  const HEIGHT = canvas.height;
  const largeurBarre = (WIDTH / tableauFrequences.length) + 2;
  let hauteurBarre; // sera défini dynamiquement en fonction de la valeur des fréquences
  let x; // sa position sur X

  function dessineBarre() {

    requestAnimationFrame(dessineBarre); 
    // permet que la fonction s'appelle elle-même 60 fois par secondes

    x = 0;
    analyseur.getByteFrequencyData(tableauFrequences); // cette méthode permet de retourner une valeur entre 0 et 255 par rapport à une fréquence donnée : ça permettra de gérer la couleur qui va de 0 à 255 en RGB

    ctx.fillStyle = "#000";
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    for(let i=0; i < frequencesAudio; i++) {
      hauteurBarre = tableauFrequences[i];
      let r = 250; // r pour red
      let g = 50; // g pour green
      let b = i; // b pour blue
      ctx.fillStyle = `rgb(${r},${g},${b})`;
      ctx.fillRect(x, HEIGHT, largeurBarre, -hauteurBarre);
      x += largeurBarre + 1; // ceci permet de créer les barres les unes à coté des autres
    }

  }

  dessineBarre();

});

// Note : Chrome a bloqué le lancement automatique (autoplay) de son ou de vidéo au lancement d'une page web, c'est pourquoi on doit tout mettre dans l'event play ici

// Pour la documentation Cf. Web Audio API


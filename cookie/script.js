
// Raccourcis clavier :
// CTRL + K + C permet de mettre en commentaire
// CTRL + K + U permet de décommenter


// Les cookies servent à récupérer nos données quand on navigue sur le web
// Les sites sont maintenant obligés de demander à l'utilisateur leur consentement pour collecter les cookies

const btn = document.querySelector('.createCookieBtn');
const btns = document.querySelectorAll('button'); 
const inputs = document.querySelectorAll('input');
const infoText = document.querySelector('.info-text');
const cookieList = document.querySelector('.cookieList');
let cookieExistant;

const today = new Date(); // attention les date ne sont pas des chaînes de caractères, il faut les transformer en string pour pouvoir les utiliser
const nextWeek = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000); // ceci permet d'ajouter 1 semaine -> on doit convertir en secondes
let day = ('0' + nextWeek).slice(9,11); // ceci permet de transformer notre objet date en string, puis slice découpe une chaîne de caractères
let month = ('0' + (today.getMonth() + 1)).slice(-2); // on doit ajouter 1 car l'index des mois commence à 0 (janvier = 0, décembre = 11)
let year = today.getFullYear();


// Mettre la date d'expiration par défaut du cookie dans 1 semaine :

document.querySelector('input[type=date]').value = `${year}-${month}-${day}`;

// On crée un objet pour y mettre les valeurs des input

btns.forEach( btn => { // btns est un array
  btn.addEventListener('click', btnAction);
})

function btnAction(e){

  let nouvelObj = {};

  inputs.forEach( input => {
    let attrName = input.getAttribute('name');
    let attrValue = attrName !== "cookieExpire" ? input.value : input.valueAsDate;
    nouvelObj[attrName] = attrValue;
    // -> méthode valueAsDate retourne une date javascript
  });

  //console.log(nouvelObj);

  let description = e.target.getAttribute('data-cookie');

  if(description === "createCookie") {
    doCreateCookie(nouvelObj.cookieName, nouvelObj.cookieValue, nouvelObj.cookieExpire);
  }
  else if (description === "showCookie") {
    doListCookie();
    }
}

function doCreateCookie(name, value, expiration) {

  infoText.innerText = ""; // ceci permet de vider l'info text
  cookieList.innerHTML = ""; // ceci permet de vider la liste affichée quand on crée un nouveau cookie

  // cookieList.childNodes.forEach(child => {
  //   child.remove();
  // }); // tous les enfants de la liste

  // Condition si le cookie a un même nom
  let cookies = document.cookie.split(';');

  cookies.forEach( cookie => {
    cookie = cookie.trim(); // trim veut dire raser en anglais -> ça permet d'enlever les espaces blancs au début et à la fin d'un élément
    //console.log(cookie) -> ceci retourne nomDuCookie=100
    let formatCookie = cookie.split('=');
    console.log(formatCookie); // ceci retourne ["nomDuCookie", "100"]

    if(formatCookie[0] === encodeURIComponent(name)) {
      cookieExistant = true;
    }
  });

  if(cookieExistant) { // si cookieExistant is true
    infoText.innerText = "Un cookie possède déjà ce nom";
    cookieExistant = false; // on le remet à false pour pouvoir écouter d'autres événements
    return;
  }

  // Condition si le cookie n'a pas de nom
  if(name.length === 0) {
    infoText.innerText = "Impossible de définir un cookie sans nom !";
    return; // -> permet de sortir de la fonction pour ne pas exécuter la suite du code
  }
  // Note : c'est une mauvaise pratique de ne pas mettre de nom au cookie

  // on encode les caractères spéciaux pour pouvoir les utiliser (par exemple les espaces blank ne seront pas remplacés par des %%%)
  // .toUTCString permet de tranformer la date en string

  document.cookie = `${encodeURIComponent(name)}=${encodeURIComponent(value)};expires=${expiration.toUTCString()}`;

  // ici le cookie a été créé -> pour le voir aller dans Inspecter > onglet Application > Cookies 

  let info = document.createElement('span');
  info.innerText = `Cookie ${name} créé !`;
  infoText.appendChild(info);
  setTimeout(() => { 
    info.remove(); // ceci enlève un élément du DOM
  }, 3000);

}

function doListCookie() {

  cookieList.innerHTML = ""; // ceci permet de mettre l'affichage à zéro

  let cookies = document.cookie.split(';');
  if(cookies.join() === "") { // join() est l'inverse de split -> transforme le tableau en châine de caractères
    infoText.innerText = "Pas de cookie à afficher";
    return;
  }

  cookies.forEach(cookie => {
    cookie = cookie.trim();
    let formatCookie = cookie.split("=");
    // console.log(formatCookie);
    let item = document.createElement('li');
    infoText.innerText = "Cliquer sur un cookie dans la liste pour le supprimer";
    item.innerText = `Nom : ${decodeURIComponent(formatCookie[0])} | Valeur : ${decodeURIComponent(formatCookie[1])}`;
    cookieList.appendChild(item);

    // Supprimer un cookie

    item.addEventListener('click', function() {
      document.cookie = `${formatCookie[0]}=; expires=${new Date(0)}`;
      item.innerText = `Cookie ${formatCookie[0]} supprimé`;
      setTimeout(() => {
          item.remove();
      }, 1000);
    })
  });
}




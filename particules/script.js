
const canvas = document.getElementById('canvas1');

// -> on prend un contexte
const ctx = canvas.getContext('2d'); // -> on précise qu'on utilise un environnement 2d
ctx.canvas.width =  window.innerWidth; // -> on respécifie que le width et le height est à 100% du window
ctx.canvas.height = window.innerHeight;
let particulesTab; // -> on crée un array pour y mettre toutes nos 100 particules


class Particule { // les classes existent depuis ES6 (en 2015), elles servent à créer des objets

  // -> le constructor sert à gérer les propriétés de mon objet :
  constructor(x, y, directionX, directionY, taille, couleur) {
    this.x = x;
    this.y = y;
    this.directionX = directionX;
    this.directionY = directionY;
    this.taille = taille;
    this.couleur = couleur;
  }

  // -> on crée des méthodes ici :
  dessine() {
    ctx.beginPath();
    ctx.arc(this.x, this.y, this.taille, 0, Math.PI * 2, false); // -> ceci permet de créer des cercles, false pour le sens horaire ou anti-horaire
    ctx.fillStyle = this.couleur;
    ctx.fill();
  }

  MAJ() {
    if(this.x + this.taille > canvas.width || this.x - this.taille < 0) { // si le rond a touché le canvas à droite, ou si le rond a touché le canvas à gauche (s'il sort du cadre sur la largeur)
      this.directionX = -this.directionX; // alors on le fait bouger dans le sens inverse
    }
    if(this.y + this.taille > canvas.height || this.y - this.taille < 0) { // si le rond a touché le plafond ou le sol (s'il sort du cadre en haut ou en bas)
      this.directionY = -this.directionY; // alors on le fait bouger dans la direction inverse
    }
    this.x += this.directionX;
    this.y += this.directionY;
    this.dessine();
  }
}


// const obj1 = new Particule(300, 300, 50, 50, 100, 'coral'); // tout est en unité pixel
// console.log(obj1);
// obj1.dessine();


function init() {

  particulesTab = [];

  for(let i = 0; i < 100; i++) { // -> on itère 100 fois pour mettre 100 objets dans le array
    let taille = (Math.random() + 0.01) * 20; // -> on ajoute 0.01 au cas où Math.random retourne 0
    let x = Math.random() * (window.innerWidth - taille * 2); // -> on enlève taille * 2 pour éviter que mon rond n'apparaisse sur les bordures de window
    let y = Math.random() * (window.innerHeight - taille * 2); // Math.random donne un nombre entre 0 et 1
    //let directionX = (Math.random() * 0.4) - 0.2; // ceci permet d'avoir un chiffre random positif ou négatif
    //let directionY = (Math.random() * 0.4) - 0.2; // on aura des valeurs comprises entre -0.2 et 0.2
    let directionX = (Math.random() * 5) - 2; // changer ces valeurs permet de modifier la vitesse des particules
    let directionY = (Math.random() * 5) - 2;
    let couleur = "white";

    particulesTab.push(new Particule(x, y, directionX, directionY, taille, couleur));
  }
}


function animation() { 
  // ceci est une fonction récursive : elle s'appelle elle-même, comment ? -> On aurait pu mettre setInterval pour qu'elle s'appelle elle-même
  // -> mais il existe une méthode plus adaptée : requestAnimationFrame
  requestAnimationFrame(animation); // requestAnimationFrame exécute une fonction 60 fois par secondes soit 60fps
  
  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight); // -> clearRect permet de nettoyer un rectangle qui part de la position (0,0) sur (x,y) jusqu'a innerWidth et innerHeigth : ici on nettoye notre écran en entier
  
  for(let i=0; i < particulesTab.length; i++) { // une fois que c'est clear on fait apparaître nos ronds
    particulesTab[i].MAJ(); // on appelle chacun de nos ronds et on y applique une méthode MAJ()
    // c'est cette méthode de mise à jour MAJ() qui permet de faire bouger nos ronds
  }
  displayText();
}


function displayText() {
  ctx.font = "150px sans-serif";
  ctx.fillStyle = "white";
  ctx.textAlign = "center";
  ctx.textBaseline = "middle";
  ctx.fillText("particules JS", window.innerWidth/2, window.innerHeight/2 - 30);
}


init();
animation();
console.log(particulesTab);

function resize() {
  init();
  animation();
}

let doIt;
window.addEventListener('resize', () => { // ceci se déclenche quand on resize l'écran
  clearTimeout(doIt); // d'abord on annule tout setTimeout existant
  doIt = setTimeout(resize, 100); // puis on appelle la fontion resize() toutes les 0.1 secondes
  ctx.canvas.width = window.innerWidth; // on dit au canvas de prendre la largeur de l'écran 
  ctx.canvas.height = window.innerHeight; // on dit au canvas de prendre la hauteur de l'écran
})


 





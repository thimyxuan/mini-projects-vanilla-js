
const form = document.querySelector('form');
const liste = document.querySelector('ul');
const input = document.querySelector('form input');
let toutesLesTaches = [];

form.addEventListener('submit', e => {
  e.preventDefault();
  const texte = input.value.trim();
  if(texte !== '') {
    rajouterUneTache(texte);
    input.value = '';
  }
});

function rajouterUneTache(texte) {
  const tache = {
    texte,
    id: Date.now() // renvoie le nombre de millisecondes écoulées depuis le 1er janvier 1970
  }
  afficherListe(tache);
}

function afficherListe(tache) {
  const item = document.createElement('li');
  item.setAttribute("data-key", tache.id);

  const input = document.createElement('input');
  input.setAttribute('type', 'checkbox');
  input.addEventListener('click', tacheFaite);
  item.appendChild(input);

  const txt = document.createElement('span');
  txt.innerText = tache.texte;
  item.appendChild(txt);

  const btn = document.createElement('button');
  btn.addEventListener('click', supprimerTache, true);

  const img = document.createElement('img');
  img.setAttribute('src', 'images/trash-regular.svg');

  btn.appendChild(img);
  item.appendChild(btn);

  liste.appendChild(item);
  toutesLesTaches.push(item);
}

function tacheFaite(e) {
  e.target.parentNode.classList.toggle("finDeTache");
}

function supprimerTache(e) {
  toutesLesTaches.forEach( el => {
    if(e.target.parentNode.getAttribute('data-key') === el.getAttribute('data-key')) {
      el.remove();
      toutesLesTaches = toutesLesTaches.filter(li => li.dataset.key !== el.dataset.key);
    }
  })
}
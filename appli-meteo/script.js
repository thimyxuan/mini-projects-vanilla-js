
const CLEFAPI = '7ff79f5ed6915a924718b62df08cc54a';
const requete = new XMLHttpRequest();
const requete2 = new XMLHttpRequest();
const methode = "GET";
 
const url = "https://api.openweathermap.org/data/2.5/forecast?q=Lyon,fr&lang=fr&units=metric&cnt=7&appid=7ff79f5ed6915a924718b62df08cc54a";
const url2 = "https://api.openweathermap.org/data/2.5/onecall?lat=45.75&lon=4.5833&lang=fr&exclude=minutely&units=metric&appid=7ff79f5ed6915a924718b62df08cc54a"

const nowContainer = document.querySelector("#current-weather");
const todayContainer = document.querySelector("#forecast-today");
const weekContainer = document.querySelector("#forecast-week");
const wholeBackground = document.querySelector("body");
const switchButton = document.querySelector("button");


window.onload = init;

function init() {
  collectApiData();
  collectApiData2();
  switchMode();
}

function collectApiData() {
  requete.open(methode, url);
  requete.onreadystatechange = function(e) {
    if(this.readyState === XMLHttpRequest.DONE) {
      if(this.status === 200) {
        const reponse = JSON.parse(this.responseText);
        const data = reponse.list;
        const now = reponse.list[0];
        const city = reponse.city.name;   
        data.forEach(buildOneHourTile);
        displayCurrentWeather(now, city);
      }
      else {
        console.log("status : " + this.status);
      }
    }
  }
  requete.send();
}

function collectApiData2() {
  requete2.open(methode, url2);
  requete2.onreadystatechange = function(e) {
    if(this.readyState === XMLHttpRequest.DONE) {
      if(this.status === 200) {
        const reponse = JSON.parse(this.responseText);
        let data = reponse.daily;
        data.pop();
        data.forEach(buildOneDayTile);
      }
      else {
        console.log("status : " + this.status);
      }
    }
  }
  requete2.send();
}


function displayCurrentWeather(now, city) {
  const options = { weekday: 'long', day: 'numeric', month: 'short', year: '2-digit' };
  const options2 = { hour: '2-digit', minute: '2-digit'}
  const today = new Date();
  nowContainer.innerHTML = `
    <div class="item">
      <img src="http://openweathermap.org/img/wn/${ now.weather[0].icon }@2x.png" class="animate__animated animate__pulse animate__slow animate__infinite" width="130px" height="auto"/>
      <div class="small-date">à ${ new Intl.DateTimeFormat('fr-FR', options2).format(today) }</div>
    </div>
    <div class="item">
      ${ now.weather[0].description }<br>
      <span class="big-temp">${ Math.round(now.main.temp)}°C</span><br>
      ${city }, <span class="small-date">${ new Intl.DateTimeFormat('fr-FR', options).format(today) }</span>
    </div>`;
}


function buildOneHourTile(hour) {
  const hourTileElement = document.createElement("div");
  hourTileElement.className = "item";
  hourTileElement.innerHTML = `${ new Date(hour.dt_txt).getHours() }h
    <br>${ Math.round(hour.main.temp) }°C`;
  todayContainer.appendChild(hourTileElement);
}


function buildOneDayTile(day) {
  const dayTileElement = document.createElement("div");
  dayTileElement.className = "item";
  const options = { weekday: 'short'};
  dayTileElement.innerHTML = `${ new Intl.DateTimeFormat('fr-FR', options).format(day.dt * 1000) }
    <br>${ Math.round(day.temp.day) }°C
    <br><img src="http://openweathermap.org/img/wn/${day.weather[0].icon}@2x.png" width="60px" height="auto"/>
  `;
  weekContainer.appendChild(dayTileElement);
}


function switchMode() {
  switchButton.addEventListener("click", function(e) {
    e.preventDefault;
    wholeBackground.classList.toggle("dark-mode");
    if(switchButton.innerHTML === "dark") {
      switchButton.innerHTML = "light";
    }
    else {
      switchButton.innerHTML = "dark";
    }
  });
}


// --------------- Éléments récupérés dans le tuto 20 projets Javascript Enzo Utariz ---------------

// Note : Désactiver l'Auto-Save dans Visual Studio Code pour éviter des appels intempestifs à l'API

let myData; 

// Récupérer la position actuelle :
function getMyPosition() {
  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition( position => {
      let lat = position.coords.latitude;
      let lon = position.coords.longitude;
      AppelAPI(lat, lon);
    });
  }
  else {
    console.log("géolocalisation non activée");
  }
}

// Fetch permet de faire un call à l'API, Fetch retourne une 'promesse'

function AppelAPI(lat, lon) {
  fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}&lang=fr&exclude=minutely&units=metric&appid=${CLEFAPI}`)
  .then((reponse) => {
    return reponse.json();
  })
  .then((data) => {
    myData = data;
  })
}

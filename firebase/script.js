
// Authentification avec Javascript : S'inscrire, se connecter, se déconnecter

// 1. Aller sur Firebase (nécessite un compte Google) > Accéder à la console > Créer un projet

// 2. Onglet Cloud Firestore > Créer une base de données

// 3. Onglet Général > Cliquer sur l'icone web </> (pas besoin de cocher Hoisting)

// 4. Copier et coller le code js dans le html au dessus de note balise <script< src="script.js"


// --------- Créer les fenêtres modales ---------

const btnInscription = document.querySelector('.btn-inscription');
const btnConnexion = document.querySelector('.btn-connexion');
const btnDeconnexion = document.querySelector('.btn-deconnexion');

const formInscription = document.querySelector('.form-inscription');
const formConnexion = document.querySelector('.form-connexion');

const emailInscription = document.querySelector('.email-inscription');
const mdpInscription = document.querySelector('.mdp-inscription');

const emailConnexion = document.querySelector('.email-connexion');
const mdpConnexion = document.querySelector('.mdp-connexion');

const info = document.querySelector('.info');
const h1 = document.querySelector("h1");
const img = document.querySelector("img");


btnInscription.addEventListener('click', () => {
  if(formConnexion.classList.contains('apparition')) {
    formConnexion.classList.remove('apparition');
  }
  formInscription.classList.toggle('apparition');
});

btnConnexion.addEventListener('click', () => {
  if(formInscription.classList.contains('apparition')) {
    formInscription.classList.remove('apparition');
  }
  formConnexion.classList.toggle('apparition');
});

// --------- Connexion : Récupérer les valeurs des inputs ---------

// Il faut activer l'enregistration par mail et mot de passe dans l'onglet Authentification dans Firebase

formInscription.addEventListener('submit', (e) => {
  e.preventDefault();
  const mailValue = emailInscription.value;
  const mdpValue = mdpInscription.value;
  auth.createUserWithEmailAndPassword(mailValue, mdpValue)
  .then(cred => { // la méthode .then retourne une promesse
    console.log(cred);
    formInscription.reset(); // reset est une méthode disponible avec les form qui permet de mettre les input à zéro
    formInscription.classList.toggle('apparition');
  });
});


// --------- Se déconnecter ---------

btnDeconnexion.addEventListener('click', (e) => {
  e.preventDefault();
  auth.signOut().then(() => {
    console.log('Déconnecté !');
  })
});


// --------- Se connecter ---------

formConnexion.addEventListener('submit', (e) => {
  e.preventDefault();
  const mailValue = emailConnexion.value;
  const mdpValue = mdpConnexion.value;
  auth.signInWithEmailAndPassword(mailValue, mdpValue)
  .then(cred => { // la méthode .then retourne une promesse
    console.log("Connecté !", cred.user);
    formConnexion.reset(); // reset est une méthode disponible avec les form qui permet de mettre les input à zéro
    formConnexion.classList.toggle('apparition');
  });
});

// --------- Gérer le contenu suivant connecté ou pas ---------

auth.onAuthStateChanged( utilisateur => {
  if(utilisateur) {
    info.innerText = "Voici le contenu privé";
    h1.innerText = "Vous voilà de retour !";
    img.setAttribute("src", "../todolist/images/pusheen-ice-cream.png");
    img.setAttribute("width", "200px");
    img.setAttribute("height", "auto");
    
  }
  else {
    console.log("Utilisateur s'est déconnecté");
    info.innerText = "Contenu public";
    h1.innerText = "Bienvenue, inscrivez-vous ou connectez-vous.";
    img.setAttribute("src", "");
    img.setAttribute("width", "");
    img.setAttribute("height", "");
  }
})
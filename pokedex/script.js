
let allPokemon = [];
let tabFinal = [];

const searchInput = document.querySelector('#recherche');
const listePokemon = document.querySelector('.liste-pokemon');
const chargement = document.querySelector('.loader');

const types = {
  normal: '#AAB09F',
  water: '#539AE2',
  electric: '#E5C531',
  fighting: '#CB5F48',
  ground: '#CC9F4F',
  psychic: '#E5709B',
  rock: '#B2A061',
  dark: '#736C75', 
  steel: '#89A1B0',
  fire: '#EA7A3C',
  grass: '#71C558',
  ice: '#70CBD4',
  poison: '#B468B7',
  flying: '#7DA6DE',
  bug: '#94BC4A',
  ghost: '#846AB6',
  dragon: '#6A7BAF',
  fairy: '#E397D1'
};


// Request API Pokemon

fetchPokemonBase();

function fetchPokemonBase() {
  fetch("https://pokeapi.co/api/v2/pokemon?limit=151")
    .then(reponse => reponse.json())
    .then((allPokemon) => {
      //console.log(allPokemon); // ceci retourne un array avec 151 objets dedans, chaque objet ayant le nom du pokemon et une url qui mène vers toutes les data du pokemon
      allPokemon.results.forEach((pokemon) => {
        fetchPokemonComplet(pokemon);
      })
    })
}

function fetchPokemonComplet(pokemon) {
  let objPokemonFull = {};
  let url = pokemon.url;
  let namePokemon = pokemon.name;

  fetch(url)
    .then(reponse => reponse.json())
    .then((pokeData) => {
      //console.log(pokeData);
      objPokemonFull.pic = pokeData.sprites.front_default;
      objPokemonFull.type = pokeData.types[0].type.name;
      objPokemonFull.id = pokeData.id;

      fetch(`https://pokeapi.co/api/v2/pokemon-species/${namePokemon}`)
        .then(reponse => reponse.json())
        .then((pokeData) => {
          //console.log(pokeData);
          objPokemonFull.name = pokeData.names[4].name;
          allPokemon.push(objPokemonFull);

          if(allPokemon.length === 151) {
            //console.log(allPokemon);
            tabFinal = allPokemon.sort((a,b) => {
              return a.id - b.id; // ceci permet de ranger par ordre de l'id
            }).slice(0,21); // on prend les 21 premiers éléments -> slice découpe et retourne un tableau
            console.log(tabFinal);

            createCard(tabFinal);
            chargement.style.display = "none";
          }
        })
    })
}


// Création des cartes

function createCard(arr) {
  for(let i=0; i<arr.length; i++) {
    const carte = document.createElement("li");

    let couleur = types[arr[i].type];
    carte.style.background = couleur;

    const txtCarte = document.createElement("h5");
    txtCarte.innerText = arr[i].name;
    
    const idCarte = document.createElement("p");
    idCarte.innerText = `id# ${arr[i].id}`;

    const imgCarte = document.createElement("img");
    imgCarte.src = arr[i].pic;

    carte.appendChild(imgCarte);
    carte.appendChild(txtCarte);
    carte.appendChild(idCarte);

    listePokemon.appendChild(carte);
  }
}


// Scroll infini

window.addEventListener('scroll', () => {
  const {scrollTop, scrollHeight, clientHeight} = document.documentElement;
  // -> ceci c'est du destructuring : on sort ces 3 éléments de document.documentElement
  // scrollTop -> c'est le scroll depuis le haut
  // srollHeight -> c'est le scroll total possible
  // clientHeight -> c'est la hauteur de la fenêtre, la partie visible
  // console.log(scrollTop, scrollHeight, clientHeight);

  if(clientHeight + scrollTop >= scrollHeight  - 20) { // ici on veut que ça se déclenche un peu avant à - 20px
    addPoke(6);
  }
});

let index = 21; // c'est le nombre de pokémons affichés au début

function addPoke(nb) {
  if(index > 151) { // si tous les pokémons sont affichés alors on sort de cette fonction
    return;
  }
  const arrToAdd = allPokemon.slice(index, index + nb); // slice (21, 27) : coupe le tableau de pokémons entre l'index 21 et 27
  createCard(arrToAdd); // on crée les nouvelles cartes à partir du nouveau tableau
  index += nb; // 27 : on met à jour l'index en ajoutant nb
}


// La recherche

searchInput.addEventListener("keyup", recherche); // la recherche est dynamique et instantanée

// ceci permet de lancer la recherche avec le bouton Rechercher :
// const formRecherche = document.querySelector("form");
// formRecherche.addEventListener('submit', (e)=> {
//   e.preventDefault();
//   recherche();
// })

function recherche() {

  if(index < 151) { // ceci permet d'afficher tous les pokémons
    addPoke(130); 
  }

  let filter, allLi, titleValue, allTitles; // on peut déclarer des variables let comme ceci

  filter = searchInput.value.toUpperCase(); // on met tout en majuscule ou tout en minuscule pour éviter les erreurs liés à la casse
  allLi = document.querySelectorAll('li');
  allTitles = document.querySelectorAll('li > h5');

  for (i=0 ; i < allLi.length; i++) {
    titleValue = allTitles[i].innerText;
    if(titleValue.toUpperCase().indexOf(filter) > -1) {
      allLi[i].style.display = "flex";
    }
    else {
      allLi[i].style.display = "none";
    }
  }
}


// Animation on input

searchInput.addEventListener('input', function(e) {
  if(e.target.value !== "") {   // -> target c'est l'input
    e.target.parentNode.classList.add('active-input');
  }
  else if (e.target.value === "") {
    e.target.parentNode.classList.remove('active-input');
  }
})
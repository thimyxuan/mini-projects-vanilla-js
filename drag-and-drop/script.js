const req = new XMLHttpRequest();
const url = "https://dog.ceo/api/breeds/image/random";
const methode = "GET";


let base = document.querySelector('.base');
const premiereCase = document.getElementById('premiere-case');
const cases = document.querySelectorAll('.case');
const destroy = document.getElementById('destroy');
const allCases = []; // -> on stocke ici toutes les cases y compris la case de base et la poubelle destroy
let choix = []; // -> on stocke ici les images retenues
let photoEnCours;
let resetButton = document.getElementById("reset");
let containerCases = document.querySelector('.container-cases');


//let indexPhoto = 1; // ceci permettra d'avoir des images aléatoires
//base.style.backgroundImage = `url('https://loremflickr.com/320/240?random=${indexPhoto}')`;


// Je choisis d'utiliser l'API Dog Api (au lieu de LoremFlickr) :
displayRandomPhoto();


// Pour ajouter un event listener c'est plus facile de mettre toutes nos cases (allCases) dans un array :
for(let i=0; i < cases.length; i++) {
  allCases.push(cases[i]);
}


// On y ajoute aussi la case poubelle destroy dans notre array :
allCases.push(destroy); 


// On ajoute des event listener sur nos cases vides :
for(const vide of allCases) {
  vide.addEventListener('dragover', dragOver);    
  vide.addEventListener('dragEnter', dragEnter);
  vide.addEventListener('drop', dragDrop);
}

// La boucle for in -> c'est pour les objets
// La boucle for of -> c'est pour les arrays
// les 3 fonctions dragOver dragEnter dragDrop vont être créées ci-dessous :


// 1. DRAPDROP :

function dragDrop() {

  if(this.id === "premiere-case") { // ceci empèche le dragdrop dans l'image de base
    return; // -> ceci permet de sortir de la fonction
  }

  if(this.id === "destroy") { // supprime l'élément quand on le drop dans la case supprimer
    base.remove();
    newBase(); // -> ceci génère une requête pour récupérer une nouvelle photo
    return; // -> ceci permet de sortir de la fonction
  }

  // ceci permet de verrouiller les éléments -> on ne pourra plus les dragger (glisser) ailleurs
  this.removeEventListener('drop', dragDrop);
  this.removeEventListener('drop', dragEnter);
  this.removeEventListener('drop', dragOver);

  this.appendChild(base); // this est l'élément que je survole
  this.childNodes[0].setAttribute('draggable', 'false'); // la base n'est plus draggable une fois placée dans la case
  newBase();

  choix.push(photoEnCours);
  if(choix.length === 3) {
    setTimeout(()=>{
      alert("Sélection terminée")
    }, 200);
    console.log(choix);
  }
}


// 2. DRAGOVER & DRAGENTER : elles servent à prendre un preventDefault afin d'autoriser le Drop

function dragOver(e) { // ceci permet d'autoriser le dragDrop
  e.preventDefault();
}

function dragEnter(e) { // ceci permet d'autoriser le dragDrop
  e.preventDefault();
}


function displayRandomPhoto() {
  req.open(methode, url);
  req.onreadystatechange = function(e) {
    if(this.readyState === XMLHttpRequest.DONE) {
      if(this.status === 200) {
        const reponse = JSON.parse(this.responseText);
        base.style.backgroundImage = `url("${reponse.message}")`;
        photoEnCours = `url("${reponse.message}")`;
      }
      else {
        console.log("status : " + this.status);
      }
    }
  }
  req.send();
}


function newBase() {
  const newBase = document.createElement("div");
  newBase.setAttribute("class", "base");
  newBase.setAttribute("draggable", "true");
  // ------ la requête pour récupérer une image random de l'API Dog Api ------
  req.open(methode, url);
  req.onreadystatechange = function(e) {
    if(this.readyState === XMLHttpRequest.DONE) {
      if(this.status === 200) {
        const reponse = JSON.parse(this.responseText);
        newBase.style.backgroundImage = `url("${reponse.message}")`;
        photoEnCours = `url("${reponse.message}")`;
      }
      else {
        console.log("status : " + this.status);
      }
    }
  }
  // ------ fin de la requête ------
  req.send();
  premiereCase.appendChild(newBase);
  base = newBase;
}


resetButton.addEventListener("click", function(e) {
  e.preventDefault; 
  choix = []; // -> on vide notre array de choix

  for(let i=0; i<containerCases.children.length; i++) {
    containerCases.children[i].innerHTML = ""; // -> on vide toutes les cases
  }

  // -> il faut aussi remettre les event listener sur les cases vidées
  for(const vide of allCases) {
    vide.addEventListener('dragover', dragOver);    
    vide.addEventListener('dragEnter', dragEnter);
    vide.addEventListener('drop', dragDrop);
  }
});




